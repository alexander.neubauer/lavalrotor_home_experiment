import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_iphone12mini.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
uuid_sensor = sensor_settings_dict["ID"]
messdaten = {uuid_sensor: {"acceleration_x": [], "acceleration_y": [], "acceleration_z": [], "timestamp": []}}
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
start_time = time.time()
while(time.time()-start_time <= measure_duration_in_s):
    data = accelerometer.acceleration
    messdaten[uuid_sensor]["acceleration_x"].append(data[0])
    messdaten[uuid_sensor]["acceleration_y"].append(data[1])
    messdaten[uuid_sensor]["acceleration_z"].append(data[2])
    messdaten[uuid_sensor]["timestamp"].append(time.time()-start_time)
    print("-")
    time.sleep(0.001)
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
with h5py.File(path_h5_file,"r+") as h5_file:

    grp_raw = h5_file.create_group("RawData")
    grp_sensor = grp_raw.create_group(uuid_sensor)
    for key in ["acceleration_x","acceleration_y","acceleration_z","timestamp"]:
        grp_sensor.create_dataset(key, data = messdaten[uuid_sensor][key]) 

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
